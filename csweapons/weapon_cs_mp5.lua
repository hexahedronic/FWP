AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "MP5"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 2
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "smg"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_smg_mp5.mdl"
SWEP.WorldModel				= "models/weapons/w_smg_mp5.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_MP5Navy.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 25
SWEP.Primary.Cone			= 0.075
SWEP.Primary.Delay 			= 0.045
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "x"

SWEP.IronSightsPos = Vector(-5.783, -5.909, 1.967)
SWEP.IronSightsAng = Vector(0, 0, 0)
