AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "P228"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "pistol"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_p228.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_p228.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_P228.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 35
SWEP.Primary.Cone			= 0.0125
SWEP.Primary.Delay 			= 0.12
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 13
SWEP.Primary.DefaultClip 	= 65
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "y"

SWEP.IronSightsPos = Vector(-5.961, 0, 2.756)
SWEP.IronSightsAng = Vector(0, 0, 0)
