AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "M249"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 4
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_mach_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_mach_m249para.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_M249.Single")
SWEP.Primary.Recoil			= 0.53
SWEP.Primary.Damage			= 30
SWEP.Primary.Cone			= 0.03
SWEP.Primary.Delay 			= 0.08
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 100
SWEP.Primary.DefaultClip 	= 300
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "ar2"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "z"

SWEP.IronSightsPos = Vector(-5.95, 0, 2.282)
SWEP.IronSightsAng = Vector(0, 0, 0)
