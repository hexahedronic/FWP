AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Galil"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_rif_galil.mdl"
SWEP.WorldModel				= "models/weapons/w_rif_galil.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_Galil.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 30
SWEP.Primary.Cone			= 0.017
SWEP.Primary.Delay 			= 0.09
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 35
SWEP.Primary.DefaultClip 	= 125
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "ar2"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "v"

SWEP.IronSightsPos = Vector(-6.37, 0, 2.598)
SWEP.IronSightsAng = Vector(0, 0, 0)
