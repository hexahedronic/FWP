AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Dual Elites"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "duel"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_elite.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_Elite.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 38
SWEP.Primary.Cone			= 0.045
SWEP.Primary.Delay 			= 0.095
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "s"
