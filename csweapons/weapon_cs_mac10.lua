AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "MAC-10"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 2
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "smg"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_smg_mac10.mdl"
SWEP.WorldModel				= "models/weapons/w_smg_mac10.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_MAC10.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 29
SWEP.Primary.Cone			= 0.075
SWEP.Primary.Delay 			= 0.055
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 130
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "l"

SWEP.IronSightsPos = Vector(-10.945, 0, 3.071)
SWEP.IronSightsAng = Vector(0.5, -8.7, -6.89)
