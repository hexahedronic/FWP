AddCSLuaFile()

SWEP.Base         = "weapon_flex_base"
SWEP.PrintName    = "AK47"
SWEP.Author       = "Flex"
SWEP.Category     = "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot    = 1
SWEP.SlotPos = 1

SWEP.HoldType = "ar2"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel	= "models/weapons/w_rif_ak47.mdl"
SWEP.UseHands   = true

SWEP.Primary.Sound   = Sound("Weapon_AK47.Single")
SWEP.Primary.Recoil  = 0.45
SWEP.Primary.Damage  = 36
SWEP.Primary.Cone    = 0.05
SWEP.Primary.Bullets = 1

SWEP.Primary.ClipSize    = 30
SWEP.Primary.DefaultClip = 120
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "ar2"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CSSelectIcons"
SWEP.SelectionLetter = "b"

SWEP.IronSightsPos = Vector(-6.535, 0, 1.81)
SWEP.IronSightsAng = Vector(2.93, 0.2, 0)
