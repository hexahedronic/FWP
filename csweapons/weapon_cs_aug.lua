AddCSLuaFile()

SWEP.Base         = "weapon_flex_base"
SWEP.PrintName    = "AUG"
SWEP.Author       = "Flex"
SWEP.Category     = "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot    = 1
SWEP.SlotPos = 1

SWEP.HoldType = "smg"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/cstrike/c_rif_aug.mdl"
SWEP.WorldModel = "models/weapons/w_rif_aug.mdl"
SWEP.UseHands   = true

SWEP.Primary.Sound   = Sound("Weapon_AUG.Single")
SWEP.Primary.Recoil  = 0.73
SWEP.Primary.Damage  = 28
SWEP.Primary.Cone    = 0.015
SWEP.Primary.Delay   = 0.09
SWEP.Primary.Bullets = 1

SWEP.Primary.ClipSize    = 30
SWEP.Primary.DefaultClip = 120
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "ar2"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CSSelectIcons"
SWEP.SelectionLetter = "e"

SWEP.IronSightsPos = Vector(-8.11, -9, 2.13)
SWEP.IronSightsAng = Vector(0.3, -1.93, -9.646)

SWEP.RifleScope = true
