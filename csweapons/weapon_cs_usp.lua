AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "USP45"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "pistol"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_usp.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_USP.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 35
SWEP.Primary.Cone			= 0.018
SWEP.Primary.Delay 			= 0.1
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "a"

SWEP.IronSightsPos = Vector(-5.907, 0, 2.598)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
	local vm = self.Owner:GetViewModel()

	if !self.Silencer and self.Owner:KeyDown(IN_USE) then
        self.Silencer = true

        self.Primary.Sound  = Sound("Weapon_USP.Silenced")
        self.Primary.Delay  = 0.1
        self.Primary.Cone   = 0.009
        self.Primary.Damage = 35
        self.Primary.Recoil	= 0.3

        self.WorldModel = "models/weapons/w_pist_usp_silencer.mdl"

        vm:SetSequence(vm:LookupSequence("add_silencer"))
        self:SetNextSecondaryFire(CurTime()+1)
    elseif self.Silencer and self.Owner:KeyDown(IN_USE) then
        self.Silencer = false

        self.Primary.Sound  = Sound("Weapon_USP.Single")
        self.Primary.Recoil	= 0.45
        self.Primary.Damage	= 35
        self.Primary.Cone   = 0.018
        self.Primary.Delay  = 0.1

        self.WorldModel = "models/weapons/w_pist_usp.mdl"

        vm:SetSequence(vm:LookupSequence("detach_silencer"))
        self:SetNextSecondaryFire(CurTime()+1)
	else
		if !self.IronSightsPos then return end
		if self:GetNextSecondaryFire() > CurTime() then return end

		bIronsights = !self:GetNWBool("Ironsights",false)

		self:EmitSound("weapons/draw_default.wav")

		self:SetIronsights(bIronsights)
		if self:GetNWBool("Ironsights") == true then
			self.Owner:SetFOV(65,0.2)
		else
			self.Owner:SetFOV(0,0.2)
		end
	end

	self:SetNextSecondaryFire(CurTime()+0.3)

end
