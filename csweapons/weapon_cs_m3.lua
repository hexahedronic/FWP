AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "M3"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 4
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "shotgun"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel				= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel				= "models/weapons/w_shot_m3super90.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_M3.Single")
SWEP.Primary.Recoil			= 2.5
SWEP.Primary.Damage			= 9
SWEP.Primary.Cone			= 0.1
SWEP.Primary.Delay 			= 0.9
SWEP.Primary.Bullets 		= 7

SWEP.Primary.ClipSize 		= 8
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "buckshot"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "k"

SWEP.Shotgun = true

SWEP.IronSightsPos = Vector(-7.639, 0, 3.542)
SWEP.IronSightsAng = Vector(0, 0, 0)
