AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "P90"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 2
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "smg"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_smg_p90.mdl"
SWEP.WorldModel				= "models/weapons/w_smg_p90.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_P90.Single")
SWEP.Primary.Recoil			= 0.51
SWEP.Primary.Damage			= 26
SWEP.Primary.Cone			= 0.013
SWEP.Primary.Delay 			= 0.066
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 50
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "m"

SWEP.IronSightsPos = Vector(-5.749, -10, 2.125)
SWEP.IronSightsAng = Vector(0, 0, 0)
