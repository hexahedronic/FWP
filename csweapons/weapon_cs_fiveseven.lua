AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Five-Seven"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "pistol"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_fiveseven.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_fiveseven.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_FiveSeven.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 32
SWEP.Primary.Cone			= 0.008
SWEP.Primary.Delay 			= 0.1
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 20
SWEP.Primary.DefaultClip 	= 120
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "u"

SWEP.IronSightsPos = Vector(-5.956, 0, 2.759)
SWEP.IronSightsAng = Vector(0, 0, 0)
