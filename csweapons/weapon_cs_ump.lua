AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "UMP"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 2
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "smg"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_smg_ump45.mdl"
SWEP.WorldModel				= "models/weapons/w_smg_ump45.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_UMP45.Single")
SWEP.Primary.Recoil			= 0.5
SWEP.Primary.Damage			= 35
SWEP.Primary.Cone			= 0.075
SWEP.Primary.Delay 			= 0.057
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "q"

SWEP.IronSightsPos = Vector(-8.841, 0, 4.28)
SWEP.IronSightsAng = Vector(-1.101, -0.401, -2.481)
