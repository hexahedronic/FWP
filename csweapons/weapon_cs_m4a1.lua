AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "M4A1"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_rif_m4a1.mdl"
SWEP.WorldModel				= "models/weapons/w_rif_m4a1.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_M4A1.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 33
SWEP.Primary.Cone			= 0.006
SWEP.Primary.Delay 			= 0.08
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 120
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "ar2"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.Silencer = false

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "w"

SWEP.IronSightsPos = Vector(-7.954, 0, 0.079)
SWEP.IronSightsAng = Vector(3.03, -1.379, -3.583)

function SWEP:SecondaryAttack()
	local vm = self.Owner:GetViewModel()

	if !self.Silencer and self.Owner:KeyDown(IN_USE) then
        self.Silencer = true

        self.Primary.Sound  = Sound("Weapon_M4A1.Silenced")
        self.Primary.Delay  = 0.1
        self.Primary.Cone   = 0.003
        self.Primary.Damage = 33
        self.Primary.Recoil	= 0.3

        self.WorldModel = "models/weapons/w_rif_m4a1_silencer.mdl"

        vm:SetSequence(vm:LookupSequence("add_silencer"))
        self:SetNextSecondaryFire(CurTime()+1)
    elseif self.Silencer and self.Owner:KeyDown(IN_USE) then
        self.Silencer = false

        self.Primary.Sound  = Sound("Weapon_M4A1.Single")
        self.Primary.Recoil	= 0.45
        self.Primary.Damage	= 33
        self.Primary.Cone   = 0.006
        self.Primary.Delay  = 0.08

        self.WorldModel = "models/weapons/w_rif_m4a1.mdl"

        vm:SetSequence(vm:LookupSequence("detach_silencer"))
        self:SetNextSecondaryFire(CurTime()+1)
	else
		if !self.IronSightsPos then return end
		if self:GetNextSecondaryFire() > CurTime() then return end

		bIronsights = !self:GetNWBool("Ironsights",false)

		self:EmitSound("weapons/draw_default.wav")

		self:SetIronsights(bIronsights)
		if self:GetNWBool("Ironsights") == true then
			self.Owner:SetFOV(65,0.2)
		else
			self.Owner:SetFOV(0,0.2)
		end
	end

	self:SetNextSecondaryFire(CurTime()+0.3)

end
