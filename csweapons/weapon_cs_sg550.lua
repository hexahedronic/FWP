AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "SG550"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 4
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel				= "models/weapons/cstrike/c_snip_sg550.mdl"
SWEP.WorldModel				= "models/weapons/w_snip_sg550.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_SG550.Single")
SWEP.Primary.Recoil			= 0.65
SWEP.Primary.Damage			= 80
SWEP.Primary.Cone			= 0
SWEP.Primary.Delay 			= 0.2
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 20
SWEP.Primary.DefaultClip 	= 110
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "357"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "o"

SWEP.IronSightsPos = Vector(-7.481, -9, 2.299)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.Scope = true
