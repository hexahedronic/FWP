AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "R8 Revolver"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "revolver"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/c_357.mdl"
SWEP.WorldModel				= "models/weapons/w_357.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Delay			= 0.75
SWEP.Primary.Sound 			= Sound("Weapon_357.Single")
SWEP.Primary.Recoil			= 0.6
SWEP.Primary.Damage			= 86
SWEP.Primary.Cone			= 0
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 8
SWEP.Primary.DefaultClip 	= 64
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "357"

SWEP.Secondary.Delay			= 0.25

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CreditsLogo"
SWEP.SelectionLetter = "e"

SWEP.IronSightsPos = Vector(-4.646, 0, 0.708)
SWEP.IronSightsAng = Vector(0, 0, 0.827)

function SWEP:PrimaryAttack()
	if self:Clip1() <= 0 then
		self:Reload()
	else
		self:EmitSound("weapons/pistol/pistol_empty.wav")
		timer.Simple(0.1,function()
			self:EmitSound("weapons/pistol/pistol_empty.wav")
			timer.Simple(0.1,function()
				self:EmitSound("weapons/pistol/pistol_empty.wav")
				timer.Simple(0.1,function()
					self:EmitSound("weapons/pistol/pistol_empty.wav")
					timer.Simple(0.1,function()
						if self.FirstShot then
							self.FirstShot = false
							self.Primary.Spread = self.Primary.Cone or 0.05
						end
						self:SetNextSecondaryFire(CurTime()+self.Primary.Delay)
						self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
						self:EmitSound(self.Primary.Sound)
						self:TakePrimaryAmmo(1)

						self:ShootBullet()
					end)
				end)
			end)
		end)
	end
end

function SWEP:SecondaryAttack()
	if self:Clip1() <= 0 then
		self:Reload()
	else
		if self.FirstShot then
			self.FirstShot = false
			self.Primary.Spread = self.Primary.Cone or 0.05
		end
		self:SetNextSecondaryFire(CurTime()+self.Secondary.Delay)
		self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		self:EmitSound(self.Primary.Sound)
		self:TakePrimaryAmmo(1)

		self:ShootBullet()
	end
end
