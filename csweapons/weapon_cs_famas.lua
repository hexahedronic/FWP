AddCSLuaFile()

--TODO:Burst Fire

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "FAMAS"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel				= "models/weapons/w_rif_famas.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_FAMAS.Single")
SWEP.Primary.Recoil			= 0.4
SWEP.Primary.Damage			= 30
SWEP.Primary.Cone			= 0.025
SWEP.Primary.Delay 			= 0.3
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 25
SWEP.Primary.DefaultClip 	= 115
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "ar2"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "t"

SWEP.IronSightsPos = Vector(-6.25, 0, 1.338)
SWEP.IronSightsAng = Vector(0, -0.276, -1.93)
