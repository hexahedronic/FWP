AddCSLuaFile()

SWEP.Base         = "weapon_flex_base"
SWEP.PrintName    = "Desert Eagle"
SWEP.Author       = "Flex"
SWEP.Category     = "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot    = 3
SWEP.SlotPos = 1

SWEP.HoldType = "revolver"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.UseHands   = true

SWEP.Primary.Sound   = Sound("Weapon_Deagle.Single")
SWEP.Primary.Recoil  = 0.2
SWEP.Primary.Damage  = 73
SWEP.Primary.Cone    = 0.01
SWEP.Primary.Delay   = 0.25
SWEP.Primary.Bullets = 1

SWEP.Primary.ClipSize    = 7
SWEP.Primary.DefaultClip = 43
SWEP.Primary.Automatic   = false
SWEP.Primary.Ammo        = "357"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CSSelectIcons"
SWEP.SelectionLetter = "f"

SWEP.IronSightsPos = Vector(-6.378, 0, 2.125)
SWEP.IronSightsAng = Vector(0, 0, 0)
