AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "XM1014"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 4
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "shotgun"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel				= "models/weapons/cstrike/c_shot_xm1014.mdl"
SWEP.WorldModel				= "models/weapons/w_shot_xm1014.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_XM1014.Single")
SWEP.Primary.Recoil			= 1.75
SWEP.Primary.Damage			= 8
SWEP.Primary.Cone			= 0.1
SWEP.Primary.Delay 			= 0.3
SWEP.Primary.Bullets 		= 7

SWEP.Primary.ClipSize 		= 7
SWEP.Primary.DefaultClip 	= 39
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "buckshot"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "B"

SWEP.Shotgun = true

SWEP.IronSightsPos = Vector(-6.901, -8.426, 2.69)
SWEP.IronSightsAng = Vector(0, -0.828, 0)
