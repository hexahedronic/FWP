AddCSLuaFile()

--TODO:Burst Fire

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Glock 18"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "pistol"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_glock18.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_Glock.Single")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 28
SWEP.Primary.Cone			= 0.014
SWEP.Primary.Delay 			= 0.1
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 30
SWEP.Primary.DefaultClip 	= 150
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "c"

SWEP.IronSightsPos = Vector(-5.776, 0, 2.95)
SWEP.IronSightsAng = Vector(0, 0, 0)
