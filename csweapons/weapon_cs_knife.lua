AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Knife"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"
SWEP.FWP_TabGroup = "CS"

SWEP.Slot 					= 3
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "knife"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel				= "models/weapons/w_knife_t.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_Knife.Slash")
SWEP.Primary.SoundHit 		= Sound("Weapon_Knife.Hit")
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 65
SWEP.Primary.Cone			= 0
SWEP.Primary.Delay 			= 0.8
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= -1
SWEP.Primary.DefaultClip 	= -1
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "j"

function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	self.Owner:ViewPunch(Angle(5,5,0))
	local tracedata = {}
	tracedata.start = self.Owner:EyePos()
	tracedata.endpos = self.Owner:EyePos()+(self.Owner:GetAimVector()*75)
	tracedata.filter = {self.Owner}
	local trace = util.TraceLine(tracedata) --I should be using hull traces here.
	if trace.Hit then
		self:SendWeaponAnim(ACT_VM_IDLE)
		local vm = self.Owner:GetViewModel()
		vm:SetSequence(vm:LookupSequence("midslash" .. math.random(1, 2)))
		self:EmitSound(self.Primary.SoundHit)
		if not trace.Entity or not trace.Entity:IsValid() then return end
		local dmg = DamageInfo()
		dmg:SetDamage(self.Primary.Damage)
		dmg:SetDamageType(DMG_CLUB)
		dmg:SetAttacker(self.Owner)
		dmg:SetInflictor(self)
		dmg:SetDamageForce(self.Owner:GetAimVector()*10)

		if not trace.Entity.TakeDamageInfo then return end
		trace.Entity:TakeDamageInfo(dmg)
	else
		self:SendWeaponAnim(ACT_VM_IDLE)
		local vm = self.Owner:GetViewModel()
		vm:SetSequence(vm:LookupSequence("midslash" .. math.random(1, 2)))
		self:EmitSound(self.Primary.Sound)
	end
end

function SWEP:SecondaryAttack()
	self:SendWeaponAnim(ACT_VM_IDLE)
	local vm = self.Owner:GetViewModel()
	vm:SetSequence(vm:LookupSequence("draw"))
end
