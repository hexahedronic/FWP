AddCSLuaFile()

ENT.Type = "anim"
ENT.PrintName = "Land Mine"
ENT.Primed = false
ENT.Delay = 2

function ENT:Initialize()
	if SERVER then
		self:SetModel("models/weapons/w_slam.mdl")
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:DrawShadow( false )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		local phys = self:GetPhysicsObject()

		if (phys:IsValid()) then
			phys:Wake()
		end
        timer.Simple(self.Delay,function()
            self.Primed = true
        end)
	end
end

function ENT:Prime(pl)
    if SERVER then
        if IsValid(pl) then
            timer.Simple(0.2,function()
                self:EmitSound("buttons/button17.wav",100,100)
                timer.Simple(0.2,function()
                    self:EmitSound("buttons/button17.wav",100,125)
                    timer.Simple(0.2,function()
                        self:EmitSound("buttons/button17.wav",100,150)
                        timer.Simple(0.2,function()
                            self:EmitSound("buttons/button17.wav",100,175)
                            timer.Simple(0.2,function()
                                local e = ents.Create("env_explosion")
                                e:SetPos(self:GetPos())
                                e:SetOwner(self:GetOwner())
                                e:Spawn()
                                e:SetKeyValue("iMagnitude",200)
                                e:Fire("Explode",0,0)
                                e:EmitSound("Weapon_AWP.Single")
                                util.BlastDamage(self,IsValid(self:GetOwner()) and self:GetOwner() or self,self:GetPos(),150,200)
                                if IsValid(self) then
                                    self:Remove()
                                end
                            end)
                        end)
                    end)
                end)
            end)
        end
    end
end

if SERVER then
    function ENT:Think()
        for _,ent in pairs(ents.FindInSphere(self:GetPos(),200)) do
            if ent:IsPlayer() or ent:GetClass() == "lua_npc_wander" then
                if self.Primed == true then
                    self:Prime(ent)
                    self.Primed = false
                end
            end
        end
    end
end