AddCSLuaFile()

local FLASHTIMER = 5 --time in seconds, for the grenade to transition from full white to clear
local EFFECT_DELAY = 2 --time, in seconds when the effects still are going on, even when the whiteness of the flash is gone (set to -1 for no effects at all =]).

local Endflash, Endflash2

ENT.Type = "anim"

function ENT:PhysicsCollide(data,phys)
	if data.Speed > 50 then
		self:EmitSound(Sound("Flashbang.Bounce"))
	end

	local impulse = -data.Speed * data.HitNormal * 0.4 + (data.OurOldVelocity * -0.6)
	phys:ApplyForceCenter(impulse)
end

if SERVER then
	local FLASH_INTENSITY = 2000

	function ENT:Initialize()

		self:SetModel("models/weapons/w_eq_flashbang_thrown.mdl")
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:DrawShadow( false )

		-- Don't collide with the player
		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		local phys = self:GetPhysicsObject()

		if (phys:IsValid()) then
			phys:Wake()
		end

		timer.Simple(2,
		function()
			if self then
				self:Explode()
			end
		end)
	end


	function ENT:Explode()

		self:EmitSound(Sound("Flashbang.Explode"))

		for _,pl in pairs(player.GetAll()) do

			local ang = self:GetAngles()

			local tracedata = {}

			tracedata.start = pl:GetShootPos()
			tracedata.endpos = self:GetPos()
			tracedata.filter = pl
			local tr = util.TraceLine(tracedata)

			if (!tr.HitWorld) then
				local dist = pl:GetShootPos():Distance( self:GetPos() )
				local endtime = FLASH_INTENSITY / (dist*2)

				if (endtime > 6) then
					endtime = 6
				elseif (endtime < 1) then
					endtime = 0
				end

				simpendtime = math.floor(endtime)
				tenthendtime = math.floor((endtime - simpendtime) * 10)

	--			if (pl:GetNWFloat("FLASHED_END") > CurTime()) then
	--				pl:SetNWFloat("FLASHED_END", endtime + pl:GetNWFloat("FLASHED_END") + CurTime() - pl:GetNWFloat("FLASHED_START"))
	--			else
					pl:SetNWFloat("FLASHED_END", endtime + CurTime())
	--			end

				pl:SetNWFloat("FLASHED_END_START", CurTime())
			end
		end
		self:Remove()
	end
end

if CLIENT then

	function ENT:Initialize()

		local Pos = self:GetPos()

		timer.Simple(2, function()
			local dynamicflash = DynamicLight( self:EntIndex() )

			if ( dynamicflash ) then
				dynamicflash.Pos = Pos
				dynamicflash.r = 255
				dynamicflash.g = 255
				dynamicflash.b = 255
				dynamicflash.Brightness = 5
				dynamicflash.Size = 1000
				dynamicflash.Decay = 1000
				dynamicflash.DieTime = CurTime() + 0.5
			end
		end)
	end

	function ENT:Draw()

		self:DrawModel()
	end

	function ENT:IsTranslucent()

		return true
	end

	local function FlashEffect() if LocalPlayer():GetNWFloat("FLASHED_END") > CurTime() then

		local pl 			= LocalPlayer()
		local FlashedEnd 		= pl:GetNWFloat("FLASHED_END")
		local FlashedStart 	= pl:GetNWFloat("FLASHED_START")

		local Alpha

		if(FlashedEnd - CurTime() > FLASHTIMER) then
			Alpha = 150
		else
			local FlashAlpha = 1 - (CurTime() - (FlashedEnd - FLASHTIMER)) / (FlashedEnd - (FlashedEnd - FLASHTIMER))
			Alpha = FlashAlpha * 150
		end

			surface.SetDrawColor(255, 255, 255, math.Round(Alpha))
			surface.DrawRect(0, 0, surface.ScreenWidth(), surface.ScreenHeight())
		end
	end

	hook.Add("HUDPaint", "FlashEffect", FlashEffect)

	local function StunEffect()
		local pl 			= LocalPlayer()
		local FlashedEnd 		= pl:GetNWFloat("FLASHED_END")
		local FlashedStart 	= pl:GetNWFloat("FLASHED_START")

		if (FlashedEnd > CurTime() and FlashedEnd - EFFECT_DELAY - CurTime() <= FLASHTIMER) then
			local FlashAlpha = 1 - (CurTime() - (FlashedEnd - FLASHTIMER)) / (FLASHTIMER)
			DrawMotionBlur( 0, FlashAlpha / ((FLASHTIMER + EFFECT_DELAY) / (FLASHTIMER * 4)), 0)

		elseif (FlashedEnd > CurTime()) then
			DrawMotionBlur( 0, 0.01, 0)
		else
			DrawMotionBlur( 0, 0, 0)
		end
	end

	hook.Add( "RenderScreenspaceEffects", "StunEffect", StunEffect )
end
