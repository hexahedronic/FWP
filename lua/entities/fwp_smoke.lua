AddCSLuaFile()

ENT.Type = "anim"

function ENT:PhysicsCollide(data,phys)
	if data.Speed > 50 then
		self:EmitSound(Sound("SmokeGrenade.Bounce"))
	end

	local impulse = -data.Speed * data.HitNormal * 0.4 + (data.OurOldVelocity * -0.6)
	phys:ApplyForceCenter(impulse)
end


function ENT:Initialize()
	if SERVER then
		self:SetModel("models/weapons/w_eq_smokegrenade.mdl")
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:DrawShadow( false )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		local phys = self:GetPhysicsObject()

		if (phys:IsValid()) then
			phys:Wake()
		end

	end
	self.timer = CurTime() + 3
end

if CLIENT then
	function ENT:Draw()
		self:DrawModel()
	end
	function ENT:IsTranslucent()
		return true
	end
end

function ENT:Think()
	if SERVER then
		if self.timer < CurTime() then
			self:EmitSound(Sound("BaseSmokeEffect.Sound"))
			self:Fire("kill", "", 15)
			self.timer = CurTime() + 20
		end
	end
	if CLIENT then
		self.SmokeTimer = self.SmokeTimer or 0

		if ( self.SmokeTimer > CurTime() ) then return end

		self.SmokeTimer = CurTime() + 0.15

		local vPos = Vector(math.Rand(-100, 100), math.Rand(-100, 100), 0);

		local vOffset = self:LocalToWorld( Vector(0, 0, self:OBBMins().z) )

		local emitter = ParticleEmitter( vOffset )

		if self.timer < CurTime() then
			local smoke = emitter:Add( "particle/particle_smokegrenade", vOffset + vPos )
			smoke:SetVelocity(VectorRand() * math.Rand(100, 350))
			smoke:SetGravity(Vector((math.Rand(-100, 100)),(math.Rand(-100, 100)),(math.Rand(0, 100))))
			smoke:SetDieTime(30)
			smoke:SetStartAlpha(math.Rand(245, 255))
			smoke:SetEndAlpha(0)
			smoke:SetStartSize(math.Rand(100, 300))
			smoke:SetEndSize(700)
			smoke:SetRoll(math.Rand(-180, 180))
			smoke:SetRollDelta(math.Rand(-0.2,0.2))
			smoke:SetColor(120, 120, 120)
			smoke:SetAirResistance(550)
		end

		emitter:Finish()
	end
end
