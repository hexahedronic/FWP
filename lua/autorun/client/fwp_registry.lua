CreateClientConVar("fwp_crosshair_r", 0  )
CreateClientConVar("fwp_crosshair_g", 255)
CreateClientConVar("fwp_crosshair_b", 0)

FWP = FWP or {}

local function FWPMenu(panel)
	local cat_color = vgui.Create("DCollapsibleCategory",panel)
	cat_color:Dock(TOP)
	cat_color:SetExpanded(1)
	cat_color:SetLabel("Colors")
	cat_color.Header:SetIcon("icon16/color_wheel.png")
	cat_color.AddItem = function(cat,ctrl) ctrl:SetParent(cat) ctrl:Dock(TOP) end

	local label_armor = vgui.Create("DLabel",panel)
	label_armor:DockMargin(5,5,0,0)
	label_armor:SetFont("DermaDefault")
	label_armor:SetText("Crosshair Color:")
	label_armor:SizeToContents()
	cat_color:AddItem(label_armor)

	local color_armor = vgui.Create("DColorMixer",panel)
	color_armor:DockMargin(5,5,0,0)
	color_armor:SetPalette(true)
	color_armor:SetAlphaBar(false)
	color_armor:SetWangs(true)
	color_armor:SetConVarR("fwp_crosshair_r")
	color_armor:SetConVarG("fwp_crosshair_g")
	color_armor:SetConVarB("fwp_crosshair_b")
	cat_color:AddItem(color_armor)
end

hook.Add( "PopulateToolMenu", "FWP.Spawnmenu",function()
	spawnmenu.AddToolMenuOption( "Options",
	"FWP",
	"FWP Settings",
	"FWP Settings",
	"",
	"",
	FWPMenu)
end)
