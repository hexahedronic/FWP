--easylua.StartWeapon("weapon_fwp_railgin")

AddCSLuaFile()

SWEP.Base         = "weapon_flex_base"
SWEP.PrintName    = "Railgun"
SWEP.Author       = "Flex"
SWEP.Category     = "FWP"

SWEP.Slot    = 4
SWEP.SlotPos = 1

SWEP.HoldType = "ar2"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"
SWEP.UseHands   = true

--Unused but for info box
SWEP.Primary.Damage = "Charge / 2"
SWEP.Primary.Recoil = 2
SWEP.Primary.Delay  = 5

SWEP.Primary.ClipSize    = 0
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic   = false
SWEP.Primary.Ammo        = "none"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CreditsLogo"
SWEP.SelectionLetter = "f"

SWEP.IronSightsPos = Vector(-7.481, -9, 2.299)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.Scope = true

function SWEP:SetupDataTables()
	self:NetworkVar("Bool", 0, "FirstShot")
	self:NetworkVar("Bool", 1, "Ironsights")
	self:NetworkVar("Int",  0, "Scope")
	self:NetworkVar("Int",  1, "Charge")
end

if CLIENT then
	function SWEP:DrawWorldModel()
		local owner = self:GetOwner()
		if not IsValid(owner) then
			self:DrawModel()
			return
		else
			self:DrawShadow(false)
			self:AddWM(owner,"models/weapons/c_models/c_tfc_sniperrifle/c_tfc_sniperrifle.mdl",Vector(1.5,-1,3),Angle(0,0,180),false,Vector(1,1,1),"")

			--Coils
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(2,-3,8),Angle(90,90,-90),false,Vector(0.25,0.25,0.25),"")
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(2,-3,12),Angle(90,90,-90),false,Vector(0.25,0.25,0.25),"")
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(2,-3,16),Angle(90,90,-90),false,Vector(0.25,0.25,0.25),"")
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(1,-3,8),Angle(-90,-90,-90),false,Vector(0.25,0.25,0.25),"")
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(1,-3,12),Angle(-90,-90,-90),false,Vector(0.25,0.25,0.25),"")
			self:AddWM(owner,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(1,-3,16),Angle(-90,-90,-90),false,Vector(0.25,0.25,0.25),"")

			--Barrel
			self:AddWM(owner,"models/props_combine/combine_fence01a.mdl",Vector(1,-8,30),Angle(90,-90,180),false,Vector(0.2,0.2,0.2),"",Color(255,42,0))
			self:AddWM(owner,"models/props_combine/combine_fence01b.mdl",Vector(1,0,30),Angle(90,-90,180),false,Vector(0.2,0.2,0.2),"",Color(255,42,0))
			self:AddWM(owner,"models/props_combine/combine_fence01a.mdl",Vector(2,0,30),Angle(90,90,180),false,Vector(0.2,0.2,0.2),"",Color(255,42,0))
			self:AddWM(owner,"models/props_combine/combine_fence01b.mdl",Vector(2,-8,30),Angle(90,90,180),false,Vector(0.2,0.2,0.2),"",Color(255,42,0))
		end
	end

	function SWEP:PostDrawViewModel(vm,ply,wep)
		if not IsValid(vm) then return end
		vm:SetMaterial("engine/occlusionproxy")
		self:AddVM(vm,"models/weapons/c_models/c_tfc_sniperrifle/c_tfc_sniperrifle.mdl",Vector(-1,-4,1),Angle(-90,0,-90),"v_weapon.awm_parent",Vector(1,1,1),"")

		--Coils
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-8,0.5),Angle(0,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-12,0.5),Angle(0,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-16,0.5),Angle(0,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-8,1.5),Angle(180,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-12,1.5),Angle(180,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")
		self:AddVM(vm,"models/weapons/c_models/c_proto_backpack/c_proto_backpack.mdl",Vector(-3,-16,1.5),Angle(180,0,-90),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"")

		--Display
		self:AddVM(vm,"models/weapons/c_models/c_builder/c_builder.mdl",Vector(-5,2,1),Angle(90,75,-90),"v_weapon.awm_parent",Vector(0.5,0.5,0.5),"",Color(30,30,30))

		--Barrel
		self:AddVM(vm,"models/props_combine/combine_fence01a.mdl",Vector(-8,-30,0),Angle(0,180,180),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"",Color(255,42,0))
		self:AddVM(vm,"models/props_combine/combine_fence01b.mdl",Vector(-8,-30,0),Angle(0,0,180),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"",Color(255,42,0))
		self:AddVM(vm,"models/props_combine/combine_fence01a.mdl",Vector(0,-30,0),Angle(0,0,180),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"",Color(255,42,0))
		self:AddVM(vm,"models/props_combine/combine_fence01b.mdl",Vector(0,-30,0),Angle(0,180,180),"v_weapon.awm_parent",Vector(0.25,0.25,0.25),"",Color(255,42,0))

		local pos,ang = vm:GetBonePosition(vm:LookupBone("v_weapon.awm_parent"))
		pos:Add(ang:Right()*-7.2)
		pos:Add(ang:Up()*-0.1)
		pos:Add(ang:Forward()*1)

		ang:RotateAroundAxis(ang:Forward(),-45)

		cam.Start3D2D(pos,ang,0.05)
			draw.DrawText(string.format("%2d",self:GetCharge()):gsub(" ","0"),"HUDNumbersGlow",0,0,Color(255,128,0),TEXT_ALIGN_CENTER)
			draw.DrawText(string.format("%2d",self:GetCharge()):gsub(" ","0"),"HUDNumbers",0,0,Color(255,128,0),TEXT_ALIGN_CENTER)
		cam.End3D2D()
	end
end

function SWEP:RailgunShot(charge)
	local bullet = {}

	bullet.Num        = 1
	bullet.Src        = self.Owner:GetShootPos()
	bullet.Dir        = self.Owner:GetAngles():Forward()
	bullet.Spread     = Vector(0,0,0)
	bullet.Tracer     = 1
	bullet.TracerName = "AirboatGunHeavyTracer"
	bullet.Force	  = 1
	bullet.Damage	  = charge
	bullet.AmmoType   = "none"

	self.Owner:FireBullets(bullet)
	self:ShootEffects()

	self.Owner:SetEyeAngles(self.Owner:EyeAngles()+Angle(2*-2,0,0))
end

local delay = CurTime()
local charge = 0
function SWEP:Think()
	local owner = self:GetOwner() or LocalPlayer()
	if SERVER then
		if owner:KeyDown(IN_ATTACK) then
			if delay and delay > CurTime() then return end
			charge = math.Clamp(charge + 1,0,99)
			self:SetCharge(charge)
			self.snd_charge:Play()
			self.snd_charge:ChangePitch(50+charge)
		elseif owner:KeyReleased(IN_ATTACK) and charge > 0 then
			if delay and delay > CurTime() then return end
			self:RailgunShot((charge+1)/2)
			self.snd_charge:Stop()
			self.snd_fire:Play()
			delay = delay + 5
			timer.Simple(1,function()
				self.snd_fire:Stop()
			end)
		else
			charge = math.Clamp(charge - 5,0,99)
			self:SetCharge(charge)
		end
	end
end

function SWEP:OnRemove()
	if CLIENT then
		local owner = self:GetOwner() or LocalPlayer()

		if IsValid(owner) then
			local vm = owner:GetViewModel() or NULL
			if IsValid(vm) then vm:SetMaterial() end
		end
	end
end

function SWEP:Holster(wep)
	self:OnRemove()
	return true
end

function SWEP:OnDrop()
	self:OnRemove()
	return true
end

local lens = Material("effects/combine_binocoverlay")

function SWEP:DrawHUD()
	if self:GetScope() != 0 then
		surface.SetMaterial(lens)
		surface.DrawTexturedRect(0,0,ScrW(),ScrH())
	end
end

function SWEP:PrimaryAttack()
	return
end

function SWEP:Initialize()
	self:SetHoldType(self.HoldType)
	self.snd_charge = CreateSound(self,"weapons/gauss/chargeloop.wav")
	self.snd_fire = CreateSound(self,"weapons/gauss/fire1.wav")
end

--easylua.EndWeapon(true,true)