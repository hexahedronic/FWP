AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Land Mine"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "slam"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/c_slam.mdl"
SWEP.WorldModel				= "models/weapons/w_slam.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Delay 			= 5

SWEP.Primary.ClipSize 		= 0
SWEP.Primary.DefaultClip 	= 1
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "slam"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CreditsLogo"
SWEP.SelectionLetter = "o"

function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire(CurTime()+self.Primary.Delay)
	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self:EmitSound(Sound("weapons/slam/throw.wav"))
	timer.Simple(0.5,function()
	end)
	self:TakePrimaryAmmo(1)
	if SERVER then
		local mine = ents.Create("fwp_mine")
		mine:SetOwner(self.Owner)
		mine:SetAngles(self.Owner:EyeAngles())

		local pos = self.Owner:GetShootPos()
		pos = pos + self.Owner:GetForward()
		pos = pos + self.Owner:GetRight()
		pos = pos + self.Owner:GetUp()
		mine:SetPos(pos)

		mine:SetOwner(self.Owner)
		mine:SetPhysicsAttacker(self.Owner)
		mine:Spawn()
		mine:Activate()

		local phys = mine:GetPhysicsObject()
		phys:SetVelocity(self.Owner:GetAimVector() * 500)
		--phys:SetMass(10000)
		phys:Wake()
		--phys:AddAngleVelocity(Vector(0, 500, 0))
	end
end