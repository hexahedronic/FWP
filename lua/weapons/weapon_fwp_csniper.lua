AddCSLuaFile()

SWEP.Base         = "weapon_flex_base"
SWEP.PrintName    = "Combine Sniper"
SWEP.Author       = "Flex"
SWEP.Category     = "FWP"
SWEP.FWP_TabGroup = "Combine"

SWEP.Slot    = 4
SWEP.SlotPos = 1

SWEP.HoldType = "ar2"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"
SWEP.UseHands   = true

SWEP.Primary.Sound   = Sound("npc/sniper/echo1.wav")
SWEP.Primary.Recoil  = 2
SWEP.Primary.Damage  = 90
SWEP.Primary.Cone    = 0
SWEP.Primary.Delay   = 1.25
SWEP.Primary.Bullets = 1

SWEP.Primary.ClipSize    = 10
SWEP.Primary.DefaultClip = 40
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "357"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CSSelectIcons"
SWEP.SelectionLetter = "r"

SWEP.IronSightsPos = Vector(-7.481, -9, 2.299)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.Scope = true

if CLIENT then
	function SWEP:DrawWorldModel()
		local owner = self:GetOwner()
		if not IsValid(owner) then
			self:DrawModel()
			return
		else
			self:DrawShadow(false)
			self:AddWM(owner,"models/weapons/w_combine_sniper.mdl",Vector(1,-4.5,22),Angle(180,0,0),false,Vector(1,1,1),"")
		end
	end

	function SWEP:PostDrawViewModel(vm,ply,wep)
		if not IsValid(vm) then return end
		vm:SetMaterial("engine/occlusionproxy")
		self:AddVM(vm,"models/weapons/w_combine_sniper.mdl",Vector(-1.5,-16,1),Angle(90,0,-90),"v_weapon.awm_parent",Vector(1,1,1),"")
	end
end

function SWEP:OnRemove()
	if CLIENT then
		local owner = self:GetOwner() or LocalPlayer()

		if IsValid(owner) then
			local vm = owner:GetViewModel() or NULL
			if IsValid(vm) then vm:SetMaterial() end
		end
	end
end

function SWEP:Holster(wep)
	self:OnRemove()
	return true
end

function SWEP:OnDrop()
	self:OnRemove()
	return true
end

function SWEP:DrawWeaponSelection(x,y,w,h,a)

	// Borders
	y = y + 10
	x = x + 10
	w = w - 20

	if self.SelectionFont == "CSSelectIcons" then y = y + 30 end

	draw.DrawText(self.SelectionLetter,self.SelectionFont,x+w/2,y,Color(0,200,200),TEXT_ALIGN_CENTER)

	// Draw weapon info box
	self:PrintWeaponInfo(x + w + 20, y + 40, a)
end

local lens = Material("effects/combine_binocoverlay")

function SWEP:DrawHUD()
	if self:GetScope() != 0 then
		surface.SetMaterial(lens)
		surface.DrawTexturedRect(0,0,ScrW(),ScrH())
	end
end