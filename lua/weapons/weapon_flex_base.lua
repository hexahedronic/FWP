AddCSLuaFile()

if CLIENT then
	surface.CreateFont("CSSelectIcons",{
		font="csd",
		weight="500",
		size=ScreenScale(60),
		antialiasing=true,
		additive=true
	})
end

SWEP.Base      = "weapon_base"
SWEP.PrintName = "Flex's Weapon Base"
SWEP.Author    = "Flex"

SWEP.Slot    = 1
SWEP.SlotPos = 1

SWEP.HoldType = "smg"

SWEP.Spawnable = false
SWEP.AdminOnly = false

SWEP.ViewModel  = "models/weapons/c_smg1.mdl"
SWEP.WorldModel	= "models/weapons/w_smg1.mdl"
SWEP.UseHands   = true

SWEP.Primary.Sound   = Sound("Weapon_SMG1.Single")
SWEP.Primary.Delay   = 0.1
SWEP.Primary.Damage  = 10
SWEP.Primary.Spread  = 0
SWEP.Primary.Recoil  = 0.5
SWEP.Primary.Bullets = 1

SWEP.Primary.ClipSize    = 6
SWEP.Primary.DefaultClip = 36
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "smg1"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"

SWEP.SelectionFont   = "CreditsLogo"
SWEP.SelectionLetter = "f"

SWEP.Scope      = false
SWEP.RifleScope = false
SWEP.Shotgun    = false

SWEP._predictedTimers = {}

if CLIENT then
	function SWEP:AddWM(owner,mdl,pos,ang,left,scale,mat,col)
		local b = "ValveBiped.Bip01_"..(left and "L" or "R").."_Hand"
		if (owner:LookupBone(b) ~= nil) then
			local bnep,bnea = owner:GetBonePosition(owner:LookupBone(b))
			bnep:Add(bnea:Right()*pos.x)
			bnep:Add(bnea:Up()*pos.y)
			bnep:Add(bnea:Forward()*pos.z)
			bnea:RotateAroundAxis(bnea:Right(),ang.x)
			bnea:RotateAroundAxis(bnea:Up(),ang.y)
			bnea:RotateAroundAxis(bnea:Forward(),ang.z)
			local m = Matrix()
			m:Scale(scale)
			local ent = ClientsideModel(mdl,RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel(mdl)
			ent:SetNoDraw(true)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:EnableMatrix("RenderMultiply",m)
			if mat then
				ent:SetMaterial(mat)
			end
			col = col or Color(255,255,255)
			render.SetColorModulation(col.r/255,col.g/255,col.b/255)
			ent:DrawModel()
			ent:Remove()
		end
	end

	function SWEP:AddVM(vm,mdl,pos,ang,bone,scale,mat,col)
		local b = tostring(bone)
		if (vm:LookupBone(b) ~= nil) then
			local bnep,bnea = vm:GetBonePosition(vm:LookupBone(b))
			bnep:Add(bnea:Right()*pos.x)
			bnep:Add(bnea:Up()*pos.y)
			bnep:Add(bnea:Forward()*pos.z)
			bnea:RotateAroundAxis(bnea:Right(),ang.x)
			bnea:RotateAroundAxis(bnea:Up(),ang.y)
			bnea:RotateAroundAxis(bnea:Forward(),ang.z)
			local m = Matrix()
			m:Scale(scale)
			local ent = ClientsideModel(mdl,RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel(mdl)
			ent:SetNoDraw(true)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:EnableMatrix("RenderMultiply",m)
			if mat then
				ent:SetMaterial(mat)
			end
			col = col or Color(255,255,255)
			render.SetColorModulation(col.r/255,col.g/255,col.b/255)
			ent:DrawModel()
			ent:Remove()
		end
	end

	local function DrawCrosshair(x,y)
		local c_col = Color(GetConVar("fwp_crosshair_r"):GetInt(),
							GetConVar("fwp_crosshair_g"):GetInt(),
							GetConVar("fwp_crosshair_b"):GetInt())
		draw.RoundedBox(0,x-28,y,20,1,c_col)
		draw.RoundedBox(0,x+8,y,20,1,c_col)
		draw.RoundedBox(0,x,y-28,1,20,c_col)
		draw.RoundedBox(0,x,y+8,1,20,c_col)

		return true
	end

	function SWEP:DoDrawCrosshair(x,y)
		if self:GetScope() != 0 or self:GetIronsights() then
			return true
		end
		return DrawCrosshair(x,y)
	end

	local arc = Material( "sprites/scope_arc" )
	local lens = Material( "overlays/scope_lens.vmt" )

	function SWEP:DrawHUD()
		if self:GetScope() != 0 then
			local screenWide, screenTall = ScrW() , ScrH()

			-- calculate the bounds in which we should draw the scope
			local inset = screenTall / 16
			local y1 = inset
			local x1 = (screenWide - screenTall) / 2 + inset
			local y2 = screenTall - inset
			local x2 = screenWide - x1

			local x = screenWide / 2
			local y = screenTall / 2

			local uv1 = 0.5 / 256
			local uv2 = 1.0 - uv1


			surface.SetDrawColor( color_black )

			--Draw the reticle with primitives
			surface.DrawLine( 0, y, screenWide, y )
			surface.DrawLine( x, 0, x, screenTall )

			-- scope dust
			surface.SetMaterial( lens )
			surface.DrawTexturedRect(x - ( ScrH() / 2 ) , 0 , ScrH() , ScrH())

			-- scope arc
			surface.SetMaterial( arc )
			-- top right
			surface.DrawTexturedRectUV(x, 0, ScrH() / 2, ScrH() / 2, 0, 1, 1, 0)
			-- top left
			surface.DrawTexturedRectUV(x - ScrH() / 2, 0, ScrH() / 2, ScrH() / 2, 1, 1, 0, 0)
			-- bottom left
			surface.DrawTexturedRectUV(x - ScrH() / 2, ScrH() / 2, ScrH() / 2, ScrH() / 2, 1, 0, 0, 1)
			-- bottom right
			surface.DrawTexturedRect(x, ScrH() / 2, ScrH() / 2, ScrH() / 2)

			surface.DrawRect(0, 0, math.ceil(x - ScrH() / 2), ScrH())
			surface.DrawRect(0, 0, math.ceil(x - ScrH() / 2), ScrH())
			surface.DrawRect(math.floor(x + ScrH() / 2), 0, math.ceil(x - ScrH() / 2), ScrH())
		end
	end
end

function SWEP:SetupDataTables()
	self:NetworkVar("Bool", 0, "FirstShot")
	self:NetworkVar("Bool", 1, "Ironsights")
	self:NetworkVar("Int",  0, "Scope")
end

function SWEP:Deploy()

end

function SWEP:DrawWeaponSelection(x,y,w,h,a)

	// Borders
	y = y + 10
	x = x + 10
	w = w - 20

	if self.SelectionFont == "CSSelectIcons" then y = y + 30 end

	draw.DrawText(self.SelectionLetter,self.SelectionFont,x+w/2,y,Color(255,220,0),TEXT_ALIGN_CENTER)

	// Draw weapon info box
	self:PrintWeaponInfo(x + w + 20, y + 40, a)
end

function SWEP:PrintWeaponInfo(x, y, alpha)

	if (self.DrawWeaponInfoBox == false) then return end

	if (self.InfoMarkup == nil) then
		local str
		local col_title = "<color=0,150,130>"
		local col_text  = "<color=255,255,255>"
		local col_green = "<color=100,200,100>"

		local ammo = ammotbl and ammotbl[self.Primary.Ammo] and ammotbl[self.Primary.Ammo] or self.Primary.Ammo

		str = "<font=HudSelectionText>"
		if self.Author != "" then str = str .. col_title .. "Author:</color> " .. col_text .. self.Author .. "</color>\n" end
		if self.Primary then
			str = str .. col_title .. "Stats:</color>\n"
			str = str .. col_text .. "\xe2\x97\x8f "..col_green.."Damage:\t"..col_text..self.Primary.Damage.."\n"
			str = str .. col_text .. "\xe2\x97\x8f "..col_green.."Recoil:\t"..col_text..self.Primary.Recoil.."\n"
			str = str .. col_text .. "\xe2\x97\x8f "..col_green.."Delay:\t"..col_text..self.Primary.Delay.."\n"
			str = str .. col_text .. "\xe2\x97\x8f "..col_green.."Clip:\t\t"..col_text..self.Primary.ClipSize.."\n"
			str = str .. col_text .. "\xe2\x97\x8f "..col_green.."Ammo:\t"..col_text..(self.Primary.Ammo == "none" and "None" or "#"..self.Primary.Ammo.."_ammo").."\n"
			str = str .. "</font>"
		end

		self.InfoMarkup = markup.Parse(str, 250)
	end

	alpha = 180

	draw.RoundedBox(8, x - 5, y - 6, 260, self.InfoMarkup:GetHeight() + 18, Color(0, 0, 0, alpha))

	self.InfoMarkup:Draw(x + 5, y + 5, nil, nil, alpha)
end

function SWEP:Initialize()
	self:SetHoldType(self.HoldType)
end

function SWEP:ShootBullet(damage,recoil,num_bullets,aimcone)
	local bullet = {}

	bullet.Num 	= self.Primary.Bullets
	bullet.Src 	= self.Owner:GetShootPos()
	bullet.Dir 	= self.Owner:GetAimVector()
	bullet.Spread 	= Vector( self.Primary.Spread, self.Primary.Spread, 0 )
	bullet.Tracer	= 5
	bullet.Force	= 1
	bullet.Damage	= self.Primary.Damage
	bullet.AmmoType = self.Primary.Ammo

	self.Owner:FireBullets(bullet)
	self:ShootEffects()

	self.Owner:SetEyeAngles(self.Owner:EyeAngles()+Angle(2*-self.Primary.Recoil,0,0))
end

function SWEP:PrimaryAttack()
	if self:Clip1() <= 0 then
		self:Reload()
	else
		if self:GetFirstShot() then
			self:SetFirstShot(false)
			self.Primary.Spread = self.Primary.Cone or 0.05
		end
		self:SetNextPrimaryFire(CurTime()+self.Primary.Delay)
		self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		self:EmitSound(self.Primary.Sound)
		self:TakePrimaryAmmo(1)

		self:ShootBullet()
	end
end

function SWEP:Reload()
	local vm = self.Owner:GetViewModel()
	if (self:Clip1() < self.Primary.ClipSize) and (self:Ammo1() > 0) then
		self:SetIronsights(false)
		self:SetScope(0)
		vm:SetNoDraw(false)
		self.Owner:SetFOV(0,0.2)
		self:SetFirstShot(true)

		self:DefaultReload(ACT_VM_RELOAD)
		self.Owner:DoAnimationEvent(ACT_RELOAD)

		self:EmitSound(Sound("Weapon_SMG1.Reload"))
	end
end

local IRONSIGHT_TIME = 0.25

function SWEP:GetViewModelPosition( pos, ang )

	if ( !self.IronSightsPos ) then return pos, ang end

	local bIron = self:GetIronsights()

	if ( bIron != self.bLastIron ) then

		self.bLastIron = bIron
		self.fIronTime = CurTime()

		if ( bIron ) then
			self.SwayScale 	= 0.3
			self.BobScale 	= 0.1
		else
			self.SwayScale 	= 1.0
			self.BobScale 	= 1.0
		end

	end

	local fIronTime = self.fIronTime or 0

	if ( !bIron && fIronTime < CurTime() - IRONSIGHT_TIME ) then
		return pos, ang
	end

	local Mul = 1.0

	if ( fIronTime > CurTime() - IRONSIGHT_TIME ) then

		Mul = math.Clamp( (CurTime() - fIronTime) / IRONSIGHT_TIME, 0, 1 )

		if (!bIron) then Mul = 1 - Mul end

	end

	local Offset	= self.IronSightsPos

	if ( self.IronSightsAng ) then

		ang = ang * 1
		ang:RotateAroundAxis( ang:Right(), 		self.IronSightsAng.x * Mul )
		ang:RotateAroundAxis( ang:Up(), 		self.IronSightsAng.y * Mul )
		ang:RotateAroundAxis( ang:Forward(), 	self.IronSightsAng.z * Mul )


	end

	local Right 	= ang:Right()
	local Up 		= ang:Up()
	local Forward 	= ang:Forward()



	pos = pos + Offset.x * Right * Mul
	pos = pos + Offset.y * Forward * Mul
	pos = pos + Offset.z * Up * Mul

	return pos, ang

end

function SWEP:SecondaryAttack()
	local vm = self.Owner:GetViewModel()

	if self.Scope then
		if self:GetScope() == 0 then
			self.Owner:SetFOV(50,0.2)
			self:SetScope(1)
			vm:SetNoDraw(true)
			self.Owner:PrintMessage(4,"2x")
			self:EmitSound("weapons/sniper/sniper_zoomin.wav")
		elseif self:GetScope() == 1 then
			self.Owner:SetFOV(30,0.2)
			self:SetScope(2)
			self.Owner:PrintMessage(4,"4x")
			self:EmitSound("weapons/sniper/sniper_zoomin.wav")
		else
			self.Owner:SetFOV(0,0.2)
			self:SetScope(0)
			vm:SetNoDraw(false)
			self:EmitSound("weapons/sniper/sniper_zoomout.wav")
		end
	elseif self.RifleScope then
		if self:GetScope() == 0 then
			self.Owner:SetFOV(50,0.2)
			self:SetScope(1)
			vm:SetNoDraw(true)
			self:EmitSound("weapons/sniper/sniper_zoomin.wav")
		elseif self:GetScope() == 1 then
			self.Owner:SetFOV(0,0.2)
			self:SetScope(0)
			vm:SetNoDraw(false)
			self:EmitSound("weapons/sniper/sniper_zoomout.wav")
		end
	else
		if !self.IronSightsPos then return end
		if self:GetNextSecondaryFire() > CurTime() then return end

		bIronsights = !self:GetIronsights()

		self:EmitSound("weapons/draw_default.wav")

		self:SetIronsights(bIronsights)
		if CLIENT then
			if self:GetIronsights() then
				self.Owner:SetFOV(65,0.2)
			else
				self.Owner:SetFOV(0,0.2)
			end
		end
	end

	self:SetNextSecondaryFire(CurTime()+0.3)

end

function SWEP:OnRestore()
	self:SetNextSecondaryFire(0)
	self:SetIronsights(false)
end