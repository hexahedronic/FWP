AddCSLuaFile()

SWEP.Base 					= "weapon_flex_base"
SWEP.PrintName 				= "Dual Deagles"
SWEP.Author 				= "Flex"
SWEP.Category 				= "FWP"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "duel"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_deagle.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("Weapon_Deagle.Single")
SWEP.Primary.Recoil			= 0.2
SWEP.Primary.Damage			= 73
SWEP.Primary.Cone			= 0.01
SWEP.Primary.Delay 			= 0.25
SWEP.Primary.Bullets 		= 1

SWEP.Primary.ClipSize 		= 14
SWEP.Primary.DefaultClip 	= 86
SWEP.Primary.Automatic 		= false
SWEP.Primary.Ammo			= "357"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CSSelectIcons"
SWEP.SelectionLetter = "f"

if CLIENT then
	function SWEP:DrawWorldModel()
		local owner = self:GetOwner()
		if not IsValid(owner) then
			self:DrawModel()
			return
		else
			self:DrawShadow(false)
		end

		if (owner:LookupBone("ValveBiped.Bip01_R_Hand") ~= nil) then
			local bnep,bnea = owner:GetBonePosition(owner:LookupBone("ValveBiped.Bip01_R_Hand"))
			bnep:Add(bnea:Right()*1)
			bnep:Add(bnea:Up()*2)
			bnep:Add(bnea:Forward()*4)
			bnea:RotateAroundAxis(bnea:Right(),180)
			bnea:RotateAroundAxis(bnea:Up(),180)
			local ent = ClientsideModel(self.WorldModel,RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel(self.WorldModel)
			ent:SetNoDraw(true)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end

		if (owner:LookupBone("ValveBiped.Bip01_L_Hand") ~= nil) then
			local bnep,bnea = owner:GetBonePosition(owner:LookupBone("ValveBiped.Bip01_L_Hand"))
			bnep:Add(bnea:Right()*1)
			bnep:Add(bnea:Up()*-2)
			bnep:Add(bnea:Forward()*4)
			bnea:RotateAroundAxis(bnea:Right(),0)
			bnea:RotateAroundAxis(bnea:Up(),0)
			local ent = ClientsideModel(self.WorldModel,RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel(self.WorldModel)
			ent:SetNoDraw(true)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end
	end

	function SWEP:PostDrawViewModel(vm,ply,wep)
		if not IsValid(vm) then return end
		vm:SetMaterial("engine/occlusionproxy")

		if (vm:LookupBone("v_weapon.elite_left") ~= nil) then
			local bnep,bnea = vm:GetBonePosition(vm:LookupBone("v_weapon.elite_left"))
			bnep:Add(bnea:Right()*-6)
			bnep:Add(bnea:Up()*-22)
			bnep:Add(bnea:Forward()*6)
			bnea:RotateAroundAxis(bnea:Forward(),-90)
			bnea:RotateAroundAxis(bnea:Up(),-90)
			local ent = ClientsideModel("models/weapons/cstrike/c_pist_deagle.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel("models/weapons/cstrike/c_pist_deagle.mdl")
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end
		if (vm:LookupBone("v_weapon.elite_right") ~= nil) then
			local bnep,bnea = vm:GetBonePosition(vm:LookupBone("v_weapon.elite_right"))
			bnep:Add(bnea:Right()*-6)
			bnep:Add(bnea:Up()*-22)
			bnep:Add(bnea:Forward()*6.5)
			bnea:RotateAroundAxis(bnea:Forward(),-90)
			bnea:RotateAroundAxis(bnea:Up(),-90)
			local ent = ClientsideModel("models/weapons/cstrike/c_pist_deagle.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end

			ent:SetModel("models/weapons/cstrike/c_pist_deagle.mdl")
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end
	end
end

function SWEP:OnRemove()
	if CLIENT then
		local owner = self:GetOwner() or LocalPlayer()

		if IsValid(owner) then
			local vm = owner:GetViewModel() or NULL
			if IsValid(vm) then vm:SetMaterial() end
		end
	end
end

function SWEP:Holster(wep)
	self:OnRemove()
	return true
end

function SWEP:OnDrop()
	self:OnRemove()
	return true
end
