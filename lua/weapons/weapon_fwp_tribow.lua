AddCSLuaFile()

SWEP.Base      = "weapon_flex_base"
SWEP.PrintName = "Grenadier's Crossbow"
SWEP.Purpose   = ""
SWEP.Author    = "Flex"
SWEP.Category  = "FWP"

SWEP.Slot 					= 1
SWEP.SlotPos 				= 1

SWEP.HoldType 				= "ar2"

SWEP.Spawnable 				= true
SWEP.AdminOnly 				= false

SWEP.ViewModel 				= "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel				= "models/weapons/w_snip_awp.mdl"
SWEP.UseHands 				= true

SWEP.Primary.Sound 			= Sound("weapons/bow_shoot.wav")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 25
SWEP.Primary.Cone			= 0
SWEP.Primary.Bullets 		= 1
SWEP.Primary.Delay          = 0.5

SWEP.Primary.ClipSize 		= 6
SWEP.Primary.DefaultClip 	= 60
SWEP.Primary.Automatic 		= true
SWEP.Primary.Ammo			= "xbowbolt"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.SelectionFont = "CreditsLogo"
SWEP.SelectionLetter = "g"

SWEP.Scope = true
SWEP.Golden = false

function SWEP:Initialize()
	self:SetHoldType(self.HoldType)
end

if CLIENT then
	function SWEP:DrawWorldModel()
		local owner = self:GetOwner()
		if not IsValid(owner) then
			self:DrawModel()
			return
		else
			self:DrawShadow(false)
		end

		if (owner:LookupBone("ValveBiped.Bip01_R_Hand") ~= nil) then
			local bnep,bnea = owner:GetBonePosition(owner:LookupBone("ValveBiped.Bip01_R_Hand"))
			bnep:Add(bnea:Right()*0)
			bnep:Add(bnea:Up()*-2)
			bnep:Add(bnea:Forward()*20)
			bnea:RotateAroundAxis(bnea:Right(),180)
			bnea:RotateAroundAxis(bnea:Up(),180)
			local ent = ClientsideModel("models/weapons/c_models/c_pro_rifle/c_pro_rifle.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end
			ent:SetModel("models/weapons/c_models/c_pro_rifle/c_pro_rifle.mdl")
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()

			bnep:Add(bnea:Up()*-1)
			bnep:Add(bnea:Forward()*-16)
			local nade = ClientsideModel("models/weapons/c_models/c_grenadelauncher/c_grenadelauncher.mdl",RENDERGROUP_OTHER)
			if not IsValid(nade) then return end
			nade:SetModel("models/weapons/c_models/c_grenadelauncher/c_grenadelauncher.mdl")
			nade:SetNoDraw(false)
			nade:SetPos(bnep)
			nade:SetAngles(bnea)
			nade:SetSkin(self.Golden and 3 or 0)
			nade:DrawModel()
			nade:Remove()

			bnep:Add(bnea:Forward()*32)
			bnep:Add(bnea:Up()*3)
			bnea:RotateAroundAxis(bnea:Forward(),90)
			local ent = ClientsideModel("models/weapons/c_models/c_bow/c_bow_thief.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end
			ent:SetModel("models/weapons/c_models/c_bow/c_bow_thief.mdl")
			ent:SetBodygroup(1,1)
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end
	end

	function SWEP:PostDrawViewModel(vm,ply,wep)
		if not IsValid(vm) then return end
		vm:SetMaterial("engine/occlusionproxy")

		if (vm:LookupBone("v_weapon.awm_parent") ~= nil) then
			local bnep,bnea = vm:GetBonePosition(vm:LookupBone("v_weapon.awm_parent"))
			bnep:Add(bnea:Right()*-3)
			bnep:Add(bnea:Up()*-16)
			bnep:Add(bnea:Forward()*0)
			bnea:RotateAroundAxis(bnea:Up(),90)
			bnea:RotateAroundAxis(bnea:Right(),-85)
			bnea:RotateAroundAxis(bnea:Forward(),0)
			local ent = ClientsideModel("models/weapons/c_models/c_pro_rifle/c_pro_rifle.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end
			ent:SetModel("models/weapons/c_models/c_pro_rifle/c_pro_rifle.mdl")
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()

			bnep:Add(bnea:Up()*-1)
			bnep:Add(bnea:Forward()*-16)
			local nade = ClientsideModel("models/weapons/c_models/c_grenadelauncher/c_grenadelauncher.mdl",RENDERGROUP_OTHER)
			if not IsValid(nade) then return end
			nade:SetModel("models/weapons/c_models/c_grenadelauncher/c_grenadelauncher.mdl")
			nade:SetNoDraw(false)
			nade:SetPos(bnep)
			nade:SetAngles(bnea)
			nade:SetSkin(self.Golden and 3 or 0)
			nade:DrawModel()
			nade:Remove()

			bnep:Add(bnea:Forward()*32)
			bnep:Add(bnea:Up()*3)
			bnea:RotateAroundAxis(bnea:Forward(),90)
			local ent = ClientsideModel("models/weapons/c_models/c_bow/c_bow_thief.mdl",RENDERGROUP_OTHER)
			if not IsValid(ent) then return end
			ent:SetModel("models/weapons/c_models/c_bow/c_bow_thief.mdl")
			ent:SetBodygroup(1,1)
			ent:SetNoDraw(false)
			ent:SetPos(bnep)
			ent:SetAngles(bnea)
			ent:DrawModel()
			ent:Remove()
		end
	end
end

function SWEP:OnRemove()
	if CLIENT then
		local owner = self:GetOwner() or LocalPlayer()

		if IsValid(owner) then
			local vm = owner:GetViewModel() or NULL
			if IsValid(vm) then vm:SetMaterial() end
		end
	end
end

function SWEP:Holster(wep)
	self:OnRemove()
	return true
end

function SWEP:OnDrop()
	self:OnRemove()
	return true
end

function SWEP:PrimaryAttack()
	if self:Clip1() <= 0 then
		self:Reload()
	else
		self:SetNextPrimaryFire(CurTime()+self.Primary.Delay)
		self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		self:EmitSound(self.Primary.Sound)
		self:TakePrimaryAmmo(1)

		if SERVER then
			local b1 = ents.Create("tf_arrow")
			b1.Owner = self.Owner
			b1.Inflictor = self.Weapon
			b1:SetOwner(self.Owner)
			local eyeang = self.Owner:GetAimVector():Angle()
			local right = eyeang:Right()
			local up = eyeang:Up()
			b1:SetPos(self.Owner:GetShootPos()+right*3-up*3)
			b1:SetAngles(self.Owner:GetAngles())
			b1:SetPhysicsAttacker(self.Owner)
			b1:Spawn()
			b1:SetMaterial(self.Golden and "models/player/shared/gold_player" or "")
			local phys = b1:GetPhysicsObject()
			if IsValid(phys) then
				phys:ApplyForceCenter(self.Owner:GetAimVector() * 1500)
			end
		end
	end
end

-- Bolt ent
local ENT = {}

ENT.Type = "anim"

function ENT:SetupDataTables()
	self:DTVar( "Entity", 1, "Ent" )
	self:DTVar( "Bool", 2, "Collided" )
end

if SERVER then
	function ENT:Initialize()

		self.Hit = false
		self.LastSound = CurTime()

		self:SetModel("models/weapons/w_models/w_arrow.mdl")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
		self:DrawShadow(false)

		local phys = self:GetPhysicsObject()
		if (phys:IsValid()) then
			phys:Wake()
			phys:EnableDrag(false)
			phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
			phys:AddGameFlag(FVPHYSICS_NO_PLAYER_PICKUP)
			phys:SetMass(1)
			phys:SetBuoyancyRatio(0)
			phys:EnableGravity(false)
		end
		self.Trail = util.SpriteTrail(self, 0, Color(255,255,255), false, 5, 1, 0.3, 0.01, "trails/laser.vmt")

	end

	function ENT:Think()

		if IsValid(self.Owner) then
			self.dt.Ent = self.Owner
			else
			self.dt.Ent = self
		end

		if self.PinData then

			local ent = self.PinData.Entity
			local rag = self.PinData.Entity
			local pos = self.PinData.Pos

			if IsValid(ent) and ent.Health and ent:Health()<=0 then

				if ent:GetMoveType() == MOVETYPE_VPHYSICS then
					rag = ents.Create("prop_physics")
				else
					rag = ents.Create("prop_ragdoll")
				end

				rag:SetModel(ent:GetModel())
				rag:SetPos(ent:GetPos())
				rag:SetAngles(ent:GetAngles())
				rag:SetColor(ent:GetColor())
				rag:SetMaterial(ent:GetMaterial())
				//rag:SetBodygroup(ent:GetBodygroup())
				rag:Spawn()
				rag:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
				rag:Fire("Kill",1,10)

				for i=0,rag:GetPhysicsObjectCount()-1 do --setup bone positions
					local bone = rag:TranslatePhysBoneToBone(i)
					local phys = rag:GetPhysicsObjectNum(i)
					if phys then
						local bpos,bang = ent:GetBonePosition(bone)
						if bpos then
							phys:SetPos(bpos)
						end
						if bang then
							phys:SetAngles(bang)
						end
						phys:SetVelocity(ent:GetVelocity())
					end
				end

				if self.PinData then
					if ent:IsNPC() then
						if IsValid(self.Owner) then
							ent:TakeDamage(1000,self.Owner,self.Owner)
						end
						ent:Remove()
					else
						ent:KillSilent()
					end
				end

			end

			if pos then

				local temptrace = {}
				temptrace.start = self:GetPos()
				temptrace.endpos = self:GetPos()+self:GetForward()*350
				temptrace.filter = {self, self.Owner, "tf_arrow"}
				temptrace.mask = MASK_SHOT-CONTENTS_SOLID
				local tr = util.TraceLine(temptrace)

				if IsValid(self) then
					self:SetPos(pos)
				end

				if IsValid(rag) then

					local bone = rag:GetPhysicsObjectNum(tr.PhysicsBone)

					if bone then
						bone:SetPos(pos)
						bone:EnableMotion(false)
						constraint.Weld(game.GetWorld(),rag,0,tr.PhysicsBone,0,false)
					end

					if self.Arrowtype == "fire" then
						rag:Ignite(10,25)
					end

				end

			end

			self.PinData = false

		end
		self:NextThink(CurTime())
		return true
	end

	function ENT:PhysicsUpdate(phys)

		if !self.Hit then

			self:SetLocalAngles(phys:GetVelocity():Angle())

			local vel = Vector(0,0,0)

			if self:WaterLevel() <= 0 then
				vel = Vector(0,0,-3)
			else
				vel = Vector(0,0,-1.5)
			end

			phys:AddVelocity(vel)

		end

	end

	function ENT:PhysicsCollide(data, physobj)

		local trace = {}
		trace.start = self:GetPos()
		trace.endpos = data.HitPos
		trace.filter = {self, self.Owner}
		trace.mask = MASK_SHOT
		trace.mins = self:OBBMins()
		trace.maxs = self:OBBMaxs()
		local tr = util.TraceHull(trace)
		local ent = data.HitEntity

		if tr.HitSky or !self:IsInWorld() then self:Remove() end

		if IsValid(self) and !self.Hit then

			if IsValid(self.Trail) then self.Trail:Fire("kill", 1, 2) end

			self.dt.Collided = true

			self:SetSolid(SOLID_NONE)
			self:SetMoveType(MOVETYPE_NONE)

			if IsValid(ent) and !data.HitEntity:IsWorld() then

				if !ent:IsPlayer() and !ent:IsNPC() and ent:GetClass() ~= "tf_arrow" then
					self:SetPos(data.HitPos)
					self:SetParent(ent)
				end

			end

			if ent:IsWorld() then
				self:EmitSound("physics/wood/wood_plank_impact_soft"..math.random(1,3)..".wav", 90, 100)
				else
				self:EmitSound("weapons/crossbow/hitbod"..math.random(1,2)..".wav", 90, 100)
			end

			local dmg = DamageInfo()

			if IsValid(self.Owner) then
				dmg:SetAttacker(self.Owner)
			else
				dmg:SetAttacker(self)
			end

			if IsValid(self.Inflictor) then
				dmg:SetInflictor(self.Inflictor)
				else
				dmg:SetInflictor(self)
			end

			dmg:SetDamagePosition(data.HitPos)
			dmg:SetDamageForce(vector_origin)
			dmg:SetDamage(20)

			if !self:GetParent():IsValid() and data.HitEntity:IsWorld() then
				self:SetPos(data.HitPos)
			end

			if IsValid(ent) then

				if ent:IsNPC() or ent:IsPlayer() then

					local tracew = {}
					tracew.start = self:GetPos()
					tracew.endpos = self:GetPos()+self:GetForward()*350
					tracew.mask = MASK_SOLID_BRUSHONLY
					local trw = util.TraceLine(tracew)

					if ent:Health() <= dmg:GetDamage() and trw.Hit then
						local pos = data.HitPos
						offpos = pos - ( data.HitPos - trw.HitPos )

						self.PinData = {}
						self.PinData.Entity = ent
						self.PinData.Pos = offpos
					end

					if !self.PinData then
						self:Remove()
					end

				end

				if !self.PinData then
					ent:TakeDamageInfo(dmg)
				end

			end

			self.Hit = true
			self:Fire("kill", 1, 60)

		end

	end

	local function DenyArrowMoving(ply, ent)
		if ent:GetClass() == "tf_arrow" then return false end
	end
	hook.Add("PhysgunPickup", "tf_arrow", DenyArrowMoving)
end

scripted_ents.Register( ENT, "tf_arrow", true )